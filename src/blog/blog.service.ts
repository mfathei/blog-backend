import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreatePostDTO } from './dto/create-post.dto';
import { Post } from './interfaces/post.interface';

@Injectable()
export class BlogService {
  constructor(@InjectModel('Post') private readonly postModel: Model<Post>) {}

  addPost(createPostDTO: CreatePostDTO): Promise<Post> {
    const newPost = new this.postModel(createPostDTO);

    return newPost.save();
  }

  getPost(postId: string): Promise<Post> {
    const post = this.postModel.findById(postId).exec();

    return post;
  }

  getAllPosts(): Promise<Post[]> {
    const posts = this.postModel.find().exec();

    return posts;
  }

  updatePost(postId: string, createPostDTO: CreatePostDTO): Promise<Post> {
    const post = this.postModel
      .findByIdAndUpdate(postId, createPostDTO, { new: true })
      .exec();

    return post;
  }

  deletePost(postId: string): Promise<Post> {
    const post = this.postModel.findByIdAndDelete(postId).exec();

    return post;
  }
}
