import * as mongoose from 'mongoose';
import { Post } from '../interfaces/post.interface';

export const BlogSchema = new mongoose.Schema<Post>({
  title: { type: String, required: true },
  description: { type: String, required: true },
  body: { type: String, required: true },
  author: { type: String, required: true },
  date_posted: { type: String, required: true },
});
