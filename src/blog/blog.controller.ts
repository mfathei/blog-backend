import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  NotFoundException,
  Post,
  Put,
  Query,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { BlogService } from './blog.service';
import { CreatePostDTO } from './dto/create-post.dto';
import { ValidateObjectId } from './shared/pipes/validate-object-id.pipes';

@Controller('blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}

  @Get('posts')
  async index() {
    const posts = await this.blogService.getAllPosts();

    return posts;
  }

  @Post('post')
  async store(@Res() res, @Body() createPostDTO: CreatePostDTO) {
    const post = await this.blogService.addPost(createPostDTO);

    return res.status(HttpStatus.CREATED).json({
      message: 'Post created successfully',
      post: post,
    });
  }

  @Put('edit')
  async editPost(
    @Res() res,
    @Query('postId', new ValidateObjectId()) postId,
    @Body() createPostDTO: CreatePostDTO,
  ) {
    const editedPost = await this.blogService.updatePost(postId, createPostDTO);

    if (!editedPost) {
      throw new NotFoundException('Post doesnot exist');
    }

    return res.status(HttpStatus.OK).json({
      message: 'Post updated successfully',
      post: editedPost,
    });
  }

  @Delete('delete')
  async deletePost(
    @Res() res,
    @Query('postId', new ValidateObjectId()) postId,
  ) {
    const deletedPost = await this.blogService.deletePost(postId);

    if (!deletedPost) {
      throw new NotFoundException('Post doesnot exist');
    }

    return res.status(HttpStatus.OK).json({
      message: 'Post deleted successfully',
      post: deletedPost,
    });
  }
}
